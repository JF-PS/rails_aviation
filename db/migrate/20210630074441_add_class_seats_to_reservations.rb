class AddClassSeatsToReservations < ActiveRecord::Migration[6.1]
  def change
    add_column :reservations, :class_seats, :string
  end
end
