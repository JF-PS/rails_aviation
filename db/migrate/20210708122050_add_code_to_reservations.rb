class AddCodeToReservations < ActiveRecord::Migration[6.1]
  def change
    add_column :reservations, :code_reservation, :integer
  end
end
