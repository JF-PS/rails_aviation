# README

## Projet SuperAirFlight (DAWIN A - Jean François Picherit Steinbrucker, Alexandre Perrot, Erwan Cadoret)

### INSTALL :

1- bundle install <br>
2- yarn add webpack <br>
3- rails db:migrate <br>
4- rails db:seed <br>

INSTALL plateform de mail : <br>
1- gem install mailcatcher <br>
2- mailcatcher

### START :

rails s


