Rails.application.routes.draw do
  resources :reservations
  #get 'flights#index'
  root to: "home#index"

  resources :flights

  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

end
