class ReservationMailer < ApplicationMailer

  def confirm_mail(user, reservation)
    @user = user
    @reservation = reservation
    mail(to: @user.email, from: "super_air_flight@localhost", subject: 'Email d\'envoie de code de réservation')
  end
end
